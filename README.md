# Content Management System

Content Management System is a blog management application.

## Installation

Use the package manager to install required dependencies.

```npm install
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)